{ lib, stdenv
, cpio
, docutils
, rust
, installShellFiles
, fetchFromGitLab
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "vtcol";
  version = "0.42.5";

  src = fetchFromGitLab {
    domain = "gitlab.com";
    owner = "phgsng";
    repo = pname;
    rev = "d6e8a37767644b50001e14ebbab594e799a94db7";
    sha256 = "sha256-fHWxiuCFO+N+ubPCS/Is64V2pLzh7xozDcRo7pQLMBg=";
  };

  cargoSha256 = "sha256-NSwn3vavrsPr+qn6oQY6kO5d9TKrxwcnqO7a3qpBphs==";

  nativeBuildInputs = [ docutils cpio installShellFiles ];

  buildFeatures = [ "vtcol-bin" ];

  cargoPatches = [ ./cargo-lock.patch ];

  target = rust.toRustTargetSpec stdenv.hostPlatform;

  # building the manpage requires docutils/rst2man
  postInstall = ''
    echo "create and install manpage"
    make man
    installManPage doc/vtcol.1.gz

    echo "create cpio archive for initrd"
    binout="target/${target}/release/${pname}"
    ls "$binout"
    mkdir -p -- tmp/usr/bin
    ls
    ls tmp
    cp -f -- "$binout" tmp/usr/bin
    pushd tmp
    <<<usr/bin/${pname} cpio -ov -H ustar >${pname}.cpio

    install -D -m644 ${pname}.cpio $out/share/vtcol/${pname}.cpio
  '';

  meta = with lib; {
    description = "Color schemes for the Linux console";
    homepage = "https://gitlab.com/phgsng/vtcol";
    license = licenses.gpl3;
    mainProgram = "vtcol";
  };
}
